using Amazon.SQS;
using BitbucketWebhooks.Domain;
using BitbucketWebhooks.Jenkins.Contract;
using BitbucketWebhooks.Jenkins.Implementation;
using BitbucketWebhooks.Jenkins.Models;
using BitbucketWebhooks.Models;
using BitbucketWebhooks.Services.Contract;
using BitbucketWebhooks.Services.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace BitbucketWebhooks.Infrastructure;

public class Services
{
    public static IServiceProvider CreateServices()
    {
        var services = new ServiceCollection();
        services.AddHttpClient();
        services.AddSingleton(typeof(Microsoft.Extensions.Logging.ILogger), GetLogger());
        services.AddSingleton<ITargetsFactory, TargetsFactory>();
        services.AddSingleton<ITargetFactory<BuildJob>, BuildJobFactory>();
        services.AddAWSService<IAmazonSQS>();
        services.AddSingleton<IQueueService, QueueService>();
        services.AddSingleton<IRepositoryMapService, RepositoryMapService>();
        services.AddSingleton<IMessageConsumer, MessageConsumer>();
        services.AddSingleton<IJenkinsClient, JenkinsClient>();
        services.Configure<ApplicationConfiguration>(GetConfiguration());
        return services.BuildServiceProvider();
    }

    private static Microsoft.Extensions.Logging.ILogger GetLogger()
    {
        var factory = new LoggerFactory().AddSerilog(Logger.Instance);
        return factory.CreateLogger<Services>();
    }

    private static IConfiguration GetConfiguration()
    {
        var builder = new ConfigurationBuilder();
        builder.AddEnvironmentVariables();
        builder.AddJsonFile("appsettings.json", true);
        return builder.Build();
    }
}
