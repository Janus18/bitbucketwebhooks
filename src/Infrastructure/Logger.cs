using Serilog;
using Serilog.Formatting.Compact;

namespace BitbucketWebhooks.Infrastructure;

public static class Logger
{
    public static readonly ILogger Instance = new LoggerConfiguration()
        .WriteTo.File(
            new CompactJsonFormatter(),
            "./logs/logs.txt",
            rollingInterval: RollingInterval.Day,
            retainedFileCountLimit: 10)
        .CreateLogger();
}