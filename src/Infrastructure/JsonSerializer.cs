using System.Text.Json;

namespace BitbucketWebhooks.Infrastructure;

public static class JsonSerializer
{
    public static readonly JsonSerializerOptions Options = new JsonSerializerOptions
    {
        WriteIndented = true
    };

    public static string Serialize(object? x) => System.Text.Json.JsonSerializer.Serialize(x, Options);

    public static T? DeserializeCamelCase<T>(string x)
    {
        var options = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
        return System.Text.Json.JsonSerializer.Deserialize<T>(x, options);
    }
}
