using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace BitbucketWebhooks.Infrastructure;

public static class Extensions
{
    public static ILogger GetLogger(this IServiceProvider provider)
    {
        return provider.GetService<ILogger>()!;
    }
}
