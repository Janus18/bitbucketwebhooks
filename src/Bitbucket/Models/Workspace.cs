namespace BitbucketWebhooks.Bitbucket.Models;

public record Workspace
{
    public string Type { get; init; } = "";
    
    public string Slug { get; init; } = "";
    
    public string Name { get; init; } = "";
    
    public string Uuid { get; init; } = "";
    
    public IDictionary<string, Link>? Links { get; init; }
}
