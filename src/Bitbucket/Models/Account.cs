using System.Text.Json.Serialization;

namespace BitbucketWebhooks.Bitbucket.Models;

public record Account
{
    [JsonPropertyName("display_name")]
    public string DisplayName { get; init; } = "";
    
    public IDictionary<string, Link>? Links { get; init; }
    
    public string Type { get; init; } = "";
    
    public string Uuid { get; init; } = "";

    [JsonPropertyName("account_id")]
    public string AccountId { get; init; } = "";

    public string? Nickname { get; init; }
}
