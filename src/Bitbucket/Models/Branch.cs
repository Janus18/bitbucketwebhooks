using System.Text.Json.Serialization;

namespace BitbucketWebhooks.Bitbucket.Models;

public record Branch
{
    [JsonPropertyName("branch")]
    public BranchInfo BranchInfo { get; init; } = null!;
    
    public Commit Commit { get; init; } = null!;
    
    public Repository Repository { get; init; } = null!;
}
