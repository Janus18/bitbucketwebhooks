namespace BitbucketWebhooks.Bitbucket.Models;

public record PullRequestMerged
{
    public Account Actor { get; init; } = null!;
    
    public PullRequest Pullrequest {get; init; } = null!;
    
    public Repository Repository {get; init; } = null!;

    public Domain.Branch? DestinationBranch
    {
        get
        {
            if (Repository?.Uuid is null)
                return null;
            if (Pullrequest?.Destination?.BranchInfo?.Name is null)
                return null;
            return new Domain.Branch(Repository.Uuid, Pullrequest.Destination.BranchInfo.Name);
        }
    }
}