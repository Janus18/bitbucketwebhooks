namespace BitbucketWebhooks.Bitbucket.Models;

public record Link
{
    public string? Href { get; init; }
}
