namespace BitbucketWebhooks.Bitbucket.Models;

public record Project
{
    public string Type { get; init; } = "";
    
    public string? Name { get; init; }
    
    public string Uuid { get; init; } = "";
    
    public IDictionary<string, Link>? Links { get; init; }
    
    public string? Key { get; init; }
}
