namespace BitbucketWebhooks.Bitbucket.Models;

public record Commit
{
    public string? Type { get; init; }
    
    public string Hash { get; init; } = "";

    public IDictionary<string, Link>? Links { get; init; }
}