using System.Text.Json.Serialization;
using BitbucketWebhooks.Domain;
using BitbucketWebhooks.Domain.JsonConverters;

namespace BitbucketWebhooks.Bitbucket.Models;

public record BranchInfo
{
    [JsonConverter(typeof(BranchNameJsonConverter))]
    public BranchName? Name { get; init; }
}
