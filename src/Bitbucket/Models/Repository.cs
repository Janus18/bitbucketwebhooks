using System.Text.Json.Serialization;
using BitbucketWebhooks.Domain;
using BitbucketWebhooks.Domain.JsonConverters;

namespace BitbucketWebhooks.Bitbucket.Models;

public record Repository
{
    public string Type { get; init; } = "";
    
    public string Name { get; init; } = "";
    
    [JsonPropertyName("full_name")]
    public string FullName { get; init; } = "";
    
    public Workspace Workspace { get; init; } = null!;
    
    [JsonConverter(typeof(RepositoryUUIDJsonConverter))]
    public RepositoryUUID? Uuid { get; init; }
    
    public IDictionary<string, Link>? Links { get; init; }
	
    public Project? Project { get; init; }

    public string Website { get; init; } = "";
    
    public string Scm { get; init; } = "";
    
    [JsonPropertyName("is_private")]
    public bool IsPrivate { get; init; }
}
