using System.Text.Json.Serialization;

namespace BitbucketWebhooks.Bitbucket.Models;

public record PullRequest
{
    public long Id { get; init; }
    
    public string Title { get; init; } = "";
    
    public string Description { get; init; } = "";
    
    public string State { get; init; } = "";
    
    public Account Author { get; init; } = null!;
    
    public Branch Source { get; init; } = null!;
    
    public Branch Destination { get; init; } = null!;

    [JsonPropertyName("merge_commit")]
    public Commit MergeCommit { get; init; } = null!;

    public IList<Account> Participants { get; init; } = null!;
    
    public IList<Account> Reviewers { get; init; } = null!;
    
    [JsonPropertyName("close_source_branch")]
    public bool CloseSourceBranch { get; init; }
    
    [JsonPropertyName("closed_by")]
    public Account ClosedBy { get; init; } = null!;
    
    public string? Reason { get; init; }
    
    [JsonPropertyName("created_on")]
    public DateTimeOffset CreatedOn{ get; init; }
    
    [JsonPropertyName("updated_on")]
    public DateTimeOffset UpdatedOn { get; init; }
    
    public object? Links{ get; init; }
}
