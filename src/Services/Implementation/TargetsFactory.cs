using BitbucketWebhooks.Domain;
using BitbucketWebhooks.Jenkins.Models;
using BitbucketWebhooks.Services.Contract;

namespace BitbucketWebhooks.Services.Implementation;

public class TargetsFactory : ITargetsFactory
{
    private readonly ITargetFactory<BuildJob> _jenkinsFactory;

    public TargetsFactory(
        ITargetFactory<BuildJob> jenkinsFactory
    )
    {
        _jenkinsFactory = jenkinsFactory;
    }

    public ITarget? CreateJenkinsTarget(Branch branch, string row)
        => _jenkinsFactory.CreateTarget(branch, row);
}
