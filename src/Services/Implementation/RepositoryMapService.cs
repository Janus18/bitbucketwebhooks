using BitbucketWebhooks.Domain;
using BitbucketWebhooks.Models;
using BitbucketWebhooks.Services.Contract;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nito.AsyncEx;

namespace BitbucketWebhooks.Services.Implementation;

public class RepositoryMapService : IRepositoryMapService
{
    private readonly AsyncLazy<RepositoryMap> _repositoryMap;

    public RepositoryMapService(
        ITargetsFactory targetsFactory,
        IOptions<ApplicationConfiguration> options,
        ILogger logger
    )
    {
        _repositoryMap = new AsyncLazy<RepositoryMap>(() => RepositoryMap.Load(options.Value.MapFileName, targetsFactory, logger));
    }

    public async Task<RepositoryMap> GetMap() => await _repositoryMap;
}
