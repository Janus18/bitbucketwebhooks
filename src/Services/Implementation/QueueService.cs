using System.Net;
using Amazon.SQS;
using Amazon.SQS.Model;
using BitbucketWebhooks.Infrastructure;
using BitbucketWebhooks.Models;
using BitbucketWebhooks.Services.Contract;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BitbucketWebhooks.Services.Implementation;

public class QueueService : IQueueService
{
    private readonly IAmazonSQS _sqs;
    private readonly ILogger _logger;
    private readonly ApplicationConfiguration _configuration;

    public QueueService(
        IAmazonSQS sqs,
        IOptions<ApplicationConfiguration> options,
        ILogger logger
    )
    {
        _sqs = sqs;
        _logger = logger;
        _configuration = options.Value;
    }

    public async Task<IList<T>> ReceiveMessages<T>() where T : class
    {
        var response = await _sqs.ReceiveMessageAsync(new ReceiveMessageRequest
        {
            QueueUrl = _configuration.QueueUrl,
            MaxNumberOfMessages = 10,
            WaitTimeSeconds = 20,
        });
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new UnexpectedHttpResponseException("Receive SQS messages", response.HttpStatusCode);
        }
        var deletedMessages = await DeleteMessages(response.Messages);
        var messages = deletedMessages.Select(m => Deserialize<T>(m.Body)).Where(x => x != null).ToList()!;
        LogMessages(messages);
        return messages!;
    }

    private async Task<IList<Message>> DeleteMessages(IList<Message> messages)
    {
        if (messages?.Any() != true)
            return new List<Message>();
        var entries = messages
            .Select(m => new DeleteMessageBatchRequestEntry(Guid.NewGuid().ToString(), m.ReceiptHandle))
            .ToList();
        var response = await _sqs.DeleteMessageBatchAsync(_configuration.QueueUrl, entries);
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new UnexpectedHttpResponseException("Delete SQS messages", response.HttpStatusCode);
        }
        var entriesGroups = entries
            .GroupBy(x => response.Failed?.Any(f => f.Id == x.Id) != true)
            .Select(x => (Success: x.Key, Values: messages.Where(m => x.Any(y => y.ReceiptHandle == m.ReceiptHandle)).ToList()))
            .ToList();
        if (entriesGroups.Any(g => !g.Success))
        {
            var messageIds = System.Text.Json.JsonSerializer.Serialize(entriesGroups.First(g => !g.Success).Values.Select(m => m.MessageId));
            _logger.LogWarning("Some messages could not be deleted. IDs: {ids}", messageIds);
        }
        return entriesGroups.Any(g => g.Success) ? entriesGroups.First(g => g.Success).Values : new List<Message>();
    }

    private T? Deserialize<T>(string from) where T : class
    {
        try
        {
            var result = JsonSerializer.DeserializeCamelCase<T>(from);
            if (result is null)
                _logger.LogError("Error deserializing message body to {type}. Body: {body}", typeof(T).Name, from);
            return result;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error deserializing message body to {type}. Body: {body}", typeof(T).Name, from);
            return null;
        }
    }

    private void LogMessages<T>(List<T> messages)
    {
        messages.ForEach(m => _logger.LogInformation("Message received: {message}", System.Text.Json.JsonSerializer.Serialize(m)));
    }
}