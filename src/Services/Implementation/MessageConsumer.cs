using BitbucketWebhooks.Bitbucket.Models;
using BitbucketWebhooks.Domain;
using BitbucketWebhooks.Services.Contract;
using Microsoft.Extensions.Logging;

namespace BitbucketWebhooks.Services.Implementation;

public class MessageConsumer : IMessageConsumer
{
    private readonly IQueueService _queueService;
    private readonly IRepositoryMapService _repositoryMapService;
    private readonly ILogger _logger;

    public MessageConsumer(
        IQueueService queueService,
        IRepositoryMapService repositoryMapService,
        ILogger logger
    )
    {
        _queueService = queueService;
        _repositoryMapService = repositoryMapService;
        _logger = logger;
    }

    public async Task Execute()
    {
        var messages = await GetMessages();
        await ExecuteTargets(messages);
    }

    private async Task<IList<PullRequestMerged>> GetMessages()
    {
        try
        {
            return await _queueService.ReceiveMessages<PullRequestMerged>();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error receiving queue messages");
            return new List<PullRequestMerged>();
        }
    }

    private async Task ExecuteTargets(IList<PullRequestMerged> pullRequests)
    {
        var map = await _repositoryMapService.GetMap();
        foreach (var pullRequest in pullRequests)
        {
            await ExecuteTarget(map.GetTarget(pullRequest), pullRequest);
        }
    }

    private async Task ExecuteTarget(ITarget target, PullRequestMerged pullRequest)
    {
        try
        {
            await target.Execute(pullRequest);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error executing target for branch {branch}", target.Branch);
        }
    }
}
