using BitbucketWebhooks.Domain;

namespace BitbucketWebhooks.Services.Contract;

public interface ITargetFactory<T> where T : ITarget
{
    T? CreateTarget(Branch branch, string line);
}
