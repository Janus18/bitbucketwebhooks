using BitbucketWebhooks.Domain;

namespace BitbucketWebhooks.Services.Contract;

public interface IRepositoryMapService
{
    Task<RepositoryMap> GetMap();
}
