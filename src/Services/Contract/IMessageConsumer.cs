namespace BitbucketWebhooks.Services.Contract;

public interface IMessageConsumer
{
    Task Execute();
}
