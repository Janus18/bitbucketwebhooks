namespace BitbucketWebhooks.Services.Contract;

public interface IQueueService
{
    Task<IList<T>> ReceiveMessages<T>() where T : class;
}
