using System.Net;

namespace BitbucketWebhooks.Models;

public class UnexpectedHttpResponseException : ApplicationException
{
    public string Action { get; set; }
    public HttpStatusCode? Code { get; set; }

    public UnexpectedHttpResponseException(string action)
        : base($"Received unexpected http response while performing \"{action}\"")
    {
        Action = action;
    }

    public UnexpectedHttpResponseException(string action, HttpStatusCode code)
        : base($"Received unexpected http response while performing \"{action}\" (status {code})")
    {
        Action = action;
        Code = code;
    }
}
