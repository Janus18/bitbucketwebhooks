namespace BitbucketWebhooks.Models;

public record ApplicationConfiguration
{
    public string QueueUrl { get; set; } = "";
    
    public string MapFileName { get; set; } = "";

    public string JenkinsUserName { get; set; } = "";

    public string JenkinsPassword { get; set; } = "";

    public string JenkinsDisableTargetKeyword { get; set; } = "";
}
