using BitbucketWebhooks.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using BitbucketWebhooks.Services.Contract;

var serviceProvider = Services.CreateServices();
serviceProvider.GetLogger().LogInformation("Starting program");
var consumer = serviceProvider.GetService<IMessageConsumer>();
while (true)
{
    serviceProvider.GetLogger().LogInformation("Executing consumer");
    await consumer!.Execute();
    Thread.Sleep(30000);
}
