using System.Text.Json;
using System.Text.Json.Serialization;

namespace BitbucketWebhooks.Domain.JsonConverters;

public class BranchNameJsonConverter : JsonConverter<BranchName>
{
    public override BranchName? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var str = reader.GetString();
        return string.IsNullOrEmpty(str) ? null : new BranchName(str);
    }

    public override void Write(Utf8JsonWriter writer, BranchName value, JsonSerializerOptions options)
        => writer.WriteStringValue(value.Value);
}
