using System.Text.Json;
using System.Text.Json.Serialization;

namespace BitbucketWebhooks.Domain.JsonConverters;

public class RepositoryUUIDJsonConverter : JsonConverter<RepositoryUUID>
{
    public override RepositoryUUID? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        => RepositoryUUID.TryParse(reader.GetString());

    public override void Write(Utf8JsonWriter writer, RepositoryUUID value, JsonSerializerOptions options)
        => writer.WriteStringValue(value.Value.ToString());
}
