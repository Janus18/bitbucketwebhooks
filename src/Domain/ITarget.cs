using BitbucketWebhooks.Bitbucket.Models;

namespace BitbucketWebhooks.Domain;

public interface ITarget
{
    Branch Branch { get; }

    Task Execute(PullRequestMerged pullRequestMerged);

    bool Valid(PullRequestMerged pullRequestMerged);
}
