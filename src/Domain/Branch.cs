namespace BitbucketWebhooks.Domain;

public record Branch(RepositoryUUID RepositoryUUID, BranchName BranchName)
{
    public static Branch Empty => new Branch(RepositoryUUID.Empty, BranchName.Empty);
}
