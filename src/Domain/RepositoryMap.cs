using BitbucketWebhooks.Bitbucket.Models;
using Microsoft.Extensions.Logging;

namespace BitbucketWebhooks.Domain;

public class RepositoryMap
{
    private readonly IList<ITarget> _targets;
    private readonly ILogger _logger;

    private RepositoryMap(IList<ITarget> targets, ILogger logger)
    {
        _targets = targets;
        _logger = logger;
    }

    public ITarget GetTarget(PullRequestMerged pullRequestMerged)
    {
        return pullRequestMerged.DestinationBranch is null
            ? new NullTarget(Branch.Empty, _logger)
            : GetTarget(pullRequestMerged.DestinationBranch);
    }

    public ITarget GetTarget(Branch branch)
    {
        return _targets.FirstOrDefault(t => t.Branch == branch) ?? new NullTarget(branch, _logger);
    }

    public static async Task<RepositoryMap> Load(string fileName, ITargetsFactory targetFactory, ILogger logger)
    {
        var lines = await File.ReadAllLinesAsync(fileName);
        var map = lines
            .Select(l => l.Split(';', 4))
            .Select(l => (UUID: new RepositoryUUID(l[0]), TargetBranch: new BranchName(l[1]), Type: ParseTarget(l[2]), Args: l[3]))
            .Select(x => x.Type is null ? null : GetTarget(targetFactory, new Branch(x.UUID, x.TargetBranch), x.Type.Value, x.Args))
            .ToList();
        return new RepositoryMap(map!, logger);
    }

    private static ITarget? GetTarget(
        ITargetsFactory targetsFactory,
        Branch branch,
        Targets target,
        string line
    )
    {
        return target switch
        {
            Targets.Jenkins => targetsFactory.CreateJenkinsTarget(branch, line),
            _               => null
        };
    }

    private static Targets? ParseTarget(string type)
    {
        if (!Enum.TryParse<Targets>(type, true, out var target))
            return null;
        return target;
    }
}
