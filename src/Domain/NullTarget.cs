using BitbucketWebhooks.Bitbucket.Models;
using Microsoft.Extensions.Logging;

namespace BitbucketWebhooks.Domain;

public class NullTarget : ITarget
{
    private readonly ILogger _logger;

    public Branch Branch { get; }

    public NullTarget(Branch branch, ILogger logger)
    {
        Branch = branch;
        _logger = logger;
    }

    public Task Execute(PullRequestMerged pullRequestMerged)
    {
        _logger.LogWarning("Branch {branch} Execute was called, but it has no target configured", Branch);
        return Task.CompletedTask;
    }

    public bool Valid(PullRequestMerged pullRequestMerged)
    {
        _logger.LogWarning("Branch {branch} Valid was called, but it has no target configured", Branch);
        return false;
    }
}
