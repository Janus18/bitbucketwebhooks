namespace BitbucketWebhooks.Domain;

public interface ITargetsFactory
{
    ITarget? CreateJenkinsTarget(Branch branch, string row);
}
