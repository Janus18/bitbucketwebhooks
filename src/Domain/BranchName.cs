namespace BitbucketWebhooks.Domain;

public record BranchName(string Value)
{
    public static BranchName Empty => new BranchName("");
}
