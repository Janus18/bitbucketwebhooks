using System.Text.RegularExpressions;

namespace BitbucketWebhooks.Domain;

public record RepositoryUUID(Guid Value)
{
    public static RepositoryUUID Empty => new RepositoryUUID(Guid.Empty);

    private static readonly Regex _nonHexadecimalRegex = new Regex("[^\\da-fA-F]+");

    public RepositoryUUID(string value) : this(Parse(value)) { }

    private static Guid Parse(string value)
    {
        if (!Guid.TryParse(_nonHexadecimalRegex.Replace(value, ""), out Guid result))
        {
            throw new ArgumentException($"Could not convert {value} to a Guid");
        }
        return result;
    }

    public override string ToString() => Value.ToString();

    public static RepositoryUUID? TryParse(string? value)
    {
        if (string.IsNullOrEmpty(value))
            return null;
        try
        {
            return new RepositoryUUID(value);
        }
        catch (Exception)
        {
            return null;
        }
    }
}
