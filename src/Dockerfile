FROM mcr.microsoft.com/dotnet/aspnet:7.0-alpine3.17-amd64 AS base
RUN apk --no-cache --no-progress upgrade && \
    apk add --no-cache --no-progress bash bind-tools curl iptables ip6tables openvpn && \
    rm -rf /tmp/*
COPY ./vpn/config.ovpn ./vpn/creds.txt /config/
COPY ./docker-entrypoint /var/task/docker-entrypoint

FROM mcr.microsoft.com/dotnet/sdk:7.0-bullseye-slim as build
WORKDIR /src
COPY ["bitbucketwebhooks.csproj", "BitbucketWebhooks/"]
RUN dotnet restore "BitbucketWebhooks/bitbucketwebhooks.csproj"

WORKDIR "/src/BitbucketWebhooks"
COPY . .
RUN dotnet build "bitbucketwebhooks.csproj" --configuration Release --output /app/build

FROM build AS publish
RUN dotnet publish "bitbucketwebhooks.csproj" \
            --configuration Release \ 
            --runtime linux-x64 \
            --self-contained false \ 
            --output /app/publish \
            -p:PublishReadyToRun=true  

FROM base AS final
WORKDIR /var/task
COPY --from=publish /app/publish .
COPY ./appsettings.json ./jobs-map.csv ./
RUN ["chmod", "+x", "./docker-entrypoint"]
ENTRYPOINT [ "./docker-entrypoint" ]
