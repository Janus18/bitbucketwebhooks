using BitbucketWebhooks.Jenkins.Models;

namespace BitbucketWebhooks.Jenkins.Contract;

public interface IJenkinsClient
{
    Task StartBuildAsync(BuildJob job);
}
