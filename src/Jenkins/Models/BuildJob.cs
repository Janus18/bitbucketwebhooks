using BitbucketWebhooks.Bitbucket.Models;
using BitbucketWebhooks.Domain;
using BitbucketWebhooks.Jenkins.Contract;
using BitbucketWebhooks.Models;

namespace BitbucketWebhooks.Jenkins.Models;

public class BuildJob : ITarget
{
    private readonly IJenkinsClient _client;
    private readonly ApplicationConfiguration _configuration;

    public Domain.Branch Branch { get; }

    public BuildJobData BuildJobData { get; }

    public BuildJob(
        IJenkinsClient jenkinsClient,
        Domain.Branch branch,
        BuildJobData buildJobData,
        ApplicationConfiguration applicationConfiguration
    )
    {
        _client = jenkinsClient;
        Branch = branch;
        BuildJobData = buildJobData;
        _configuration = applicationConfiguration;
    }

    public override string ToString() => BuildJobData.ToString();

    public Task Execute(PullRequestMerged pullRequestMerged)
    {
        if (!Valid(pullRequestMerged))
            return Task.CompletedTask;
        return _client.StartBuildAsync(this);
    }

    public bool Valid(PullRequestMerged pullRequestMerged)
    {
        return pullRequestMerged.DestinationBranch == Branch &&
            pullRequestMerged.Pullrequest?.Description?.Contains(_configuration.JenkinsDisableTargetKeyword) != true;
    }
}
