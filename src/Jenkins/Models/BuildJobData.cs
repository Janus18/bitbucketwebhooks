using BitbucketWebhooks.Infrastructure;

namespace BitbucketWebhooks.Jenkins.Models;

public record BuildJobData(Uri Uri, IDictionary<string, string>? Parameters)
{
    public HttpContent? HttpContent
    {
        get
        {
            if (Parameters is null || Parameters.Any())
                return null;
            var content = new MultipartFormDataContent();
            Parameters.ToList().ForEach(x => content.Add(new StringContent(x.Value), x.Key));
            return content;
        }
    }

    public override string ToString()
        => $"{Uri} {(Parameters is null ? "" : JsonSerializer.Serialize(Parameters))}";
}
