using System.Web;
using BitbucketWebhooks.Domain;
using BitbucketWebhooks.Jenkins.Contract;
using BitbucketWebhooks.Jenkins.Models;
using BitbucketWebhooks.Models;
using BitbucketWebhooks.Services.Contract;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BitbucketWebhooks.Jenkins.Implementation;

public class BuildJobFactory : ITargetFactory<BuildJob>
{
    private readonly IJenkinsClient _client;
    private readonly ApplicationConfiguration _configuration;
    private readonly ILogger _logger;

    public BuildJobFactory(
        IJenkinsClient client,
        IOptions<ApplicationConfiguration> options,
        ILogger logger
    )
    {
        _client = client;
        _configuration = options.Value;
        _logger = logger;
    }

    public BuildJob? CreateTarget(Branch branch, string line)
    {
        var data = GetBuildJobData(line);
        return data == null
            ? null
            : new BuildJob(_client, branch, data, _configuration);
    }

    private BuildJobData? GetBuildJobData(string line)
    {
        var uri = GetUri(line);
        return uri == null
            ? null
            : new BuildJobData(new Uri(uri.GetLeftPart(UriPartial.Path)), GetParameters(uri));
    }

    private Uri? GetUri(string line)
    {
        try
        {
            return new Uri(line);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Could not read line {line} as URI", line);
            return null;
        }
    }

    private IDictionary<string, string>? GetParameters(Uri uri)
    {
        try
        {
            var parsed = HttpUtility.ParseQueryString(uri.Query);
            return parsed.AllKeys
                .Where(key => !string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(parsed[key]))
                .ToDictionary(key => key!, key => parsed[key]!);
        }
        catch (Exception)
        {
            return null;
        }
    }
}
