using System.Net.Http.Headers;
using BitbucketWebhooks.Jenkins.Contract;
using BitbucketWebhooks.Jenkins.Models;
using BitbucketWebhooks.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BitbucketWebhooks.Jenkins.Implementation;

public class JenkinsClient : IJenkinsClient
{
    private readonly ApplicationConfiguration _configuration;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly ILogger _logger;

    public JenkinsClient(
        IHttpClientFactory httpClientFactory,
        IOptions<ApplicationConfiguration> options,
        ILogger logger
    )
    {
        _configuration = options.Value;
        _httpClientFactory = httpClientFactory;
        _logger = logger;
    }

    public async Task StartBuildAsync(BuildJob job)
    {
        var client = _httpClientFactory.CreateClient();
        try
        {
            _logger.LogInformation("Sending request to build {uri}", job.BuildJobData.Uri);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AuthenticationHeaderValue());
            await client.PostAsync(job.BuildJobData.Uri, job.BuildJobData.HttpContent);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error starting Jenkins build");
        }
    }

    private string AuthenticationHeaderValue()
    {
        var authenticationString = $"{_configuration.JenkinsUserName}:{_configuration.JenkinsPassword}";
        return Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));
    }
}
