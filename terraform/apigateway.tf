resource "aws_apigatewayv2_api" "apigateway" {
  cors_configuration {
    allow_headers = ["*"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }
  name          = "bitbucketwebhooks"
  protocol_type = "HTTP"
  tags          = local.common_tags
}

data "aws_iam_policy_document" "apigateway_assume_role_policy" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "apigateway_sqs_send_policy" {
  statement {
    effect = "Allow"
    actions = ["sqs:SendMessage"]
    resources = [ aws_sqs_queue.bitbucketwebhooks_queue.arn ]
  }
}

resource "aws_iam_role" "instance" {
  name               = "bitbucket_apigateway_role"
  path               = "/system/"
  assume_role_policy = data.aws_iam_policy_document.apigateway_assume_role_policy.json
  inline_policy {
    name = "Allow_SQS_send_message"
    policy = data.aws_iam_policy_document.apigateway_sqs_send_policy.json
  }
}

resource "aws_apigatewayv2_integration" "apigateway_sqs_integration" {
  api_id               = aws_apigatewayv2_api.apigateway.id
  integration_type     = "AWS_PROXY"
  integration_subtype  = "SQS-SendMessage"

  connection_type      = "INTERNET"
  credentials_arn      = aws_iam_role.instance.arn
  description          = "Integration to SQS"
  request_parameters = {
    "QueueUrl"    = aws_sqs_queue.bitbucketwebhooks_queue.url
    "MessageBody" = "$request.body"
  }
}

resource "aws_apigatewayv2_route" "apigateway_sqs_route" {
  api_id    = aws_apigatewayv2_api.apigateway.id
  route_key = "POST /webhooks"
  target    = "integrations/${aws_apigatewayv2_integration.apigateway_sqs_integration.id}"
}

resource "aws_apigatewayv2_stage" "defaultstage" {
  api_id      = aws_apigatewayv2_api.apigateway.id
  name        = "default"
  auto_deploy = true
  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.bitbucketwebhooks.arn
    format          = "{ \"requestId\":\"$context.requestId\", \"extendedRequestId\":\"$context.extendedRequestId\", \"ip\": \"$context.identity.sourceIp\", \"caller\":\"$context.identity.caller\", \"user\":\"$context.identity.user\", \"requestTime\":\"$context.requestTime\", \"httpMethod\":\"$context.httpMethod\", \"resourcePath\":\"$context.resourcePath\", \"status\":\"$context.status\", \"protocol\":\"$context.protocol\", \"responseLength\":\"$context.responseLength\", \"error\": { \"message\": \"$context.error.message\", \"integrationMessage\": \"$context.integrationErrorMessage\" } }"
  }
  tags = local.common_tags
}
