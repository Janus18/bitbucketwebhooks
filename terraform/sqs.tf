resource "aws_sqs_queue" "bitbucketwebhooks_queue" {
  name                      = "bitbucketwebhooks-queue"
  max_message_size          = 262144
  message_retention_seconds = 900
  tags = local.common_tags
}

