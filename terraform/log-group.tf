resource "aws_cloudwatch_log_group" "bitbucketwebhooks" {
  name              = "bitbucketwebhooks"
  retention_in_days = 7
  tags = local.common_tags
  lifecycle {
    prevent_destroy = false
  }
}
