# BitbucketWebhooks

Receive Bitbucket webhooks on pull-requests merged and execute
an action.  
Although the project is extensible, currently the only action
(or target in the application) is starting a Jenkins pipeline.  
It is intended to be used as a replacement for runners when
those cannot be used.  

## Implementation overview
The webhooks are received on an AWS API Gateway. The gateway
uses an integration to send the request body to AWS SQS. The
queue is consumed by a [dotnet application](/-/tree/main/src),
which could be hosted in the cloud or on-premises.  
In the real world use case, the Jenkins instance is behind a
VPN, so the Dockerfile for the dotnet application, apart from
building and executing the application, connects to a VPN.
Right now, this is not optional.

## Terraform
There's a [terraform folder](/-/tree/main/terraform) with the
definitions of the minimum AWS resources needed.  
The resources which would be created are:
* An API Gateway to receive the webhooks.
* An SQS queue to store the webhooks.
* A log group used by the API Gateway.
* A role which allows the gateway integration to send messages
to SQS.

## Application
As said, the application only really expects pull-request
merged webhooks. The body of those messages, among other
fields, contains the repository UUID and the destination
branch. These values are used as the identifier for `Targets`.
Target objects could contain any action we want to execute
when the webhook is received. Currently, the only targets
are `BuildJobs`, which will trigger a Jenkins pipeline.  
Of course we need to configure which target is used for each
of the webhooks received. A file named `jobs-map.csv` must be
placed next to the executable, with one line for each of the
repository/branch configurations. The format of a line in the
file is
```
[RepositoryUUID];[BranchName];Jenkins;https://localhost:3000/?BRANCH_NAME=master
```
* `RepositoryUUID` and `BranchName` will match with the values
in the webhook body.
* The next value (`Jenkins`) indicates which
kind of target will be built, in this case a `BuildJob`.
* The last value depends on the kind of target; each kind
must define a parser for this value. For Jenkins, it must
be a URL with optional queries, each query will be sent
as an argument to the pipeline.

## Build and run the docker image
The Dockerfile expects a `vpn` folder to be present in the
build context. The folder must contain two files:
* `config.ovpn` with the full vpn configuration, ready to be
used by openvpn.
* `creds.txt` with the credentials for the vpn (should be
referenced by `config.ovpn` as well).
To run the image, use a command like this:
```
docker run -it \
    --cap-add=NET_ADMIN \
    --device=/dev/net/tun \
    --name webhooks \
    -e AWS_ACCESS_KEY_ID=*** \
    -e AWS_SECRET_ACCESS_KEY=*** \
    -e AWS_REGION='us-east-1' \
    -e JenkinsUserName=*** \
    -e JenkinsPassword=*** \
    -e QueueUrl='https://sqs.us-east-1.amazonaws.com/***' \
    webhooks
```
